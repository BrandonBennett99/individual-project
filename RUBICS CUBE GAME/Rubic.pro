############################################################################################################
# This code is used for the Individual Project of Antreas Josephides ID:201026404
############################################################################################################

TEMPLATE = app
TARGET = Rubic
INCLUDEPATH += . /opt/local/include

QT += widgets opengl gui

LIBS += -lglut -lGLU

# Input
HEADERS += Image.h RubicWidget.h RubicCubeWindow.h
SOURCES += Main.cpp \
           RubicWidget.cpp \
           RubicCubeWindow.cpp \
           Image.cpp
