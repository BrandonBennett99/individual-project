/****************************************************************************
** This file incluses coding for every additional function used on the software
** This code is part of Individual Project Software
** Antreas Josephides ID :201026404
** and used for Coursework 2 of Computer Graphics
*****************************************************************************/
#ifndef __GL_POLYGON_WIDGET_H__
#define __GL_POLYGON_WIDGET_H__ 1

#include <QGLWidget>
#include <GL/glut.h>
#include <QImage>
#include "Image.h"


class RubicWidget: public QGLWidget
{

	Q_OBJECT

	public:

	RubicWidget(QWidget *parent);
	~RubicWidget();

	public slots:
	void updateXrotation(int i);

	public slots:
  void updateYrotation(int i);

	public slots:
	void updateMethod1Moves();
	void updateMethod2Moves();


	protected:

	void initializeGL(); // called when OpenGL context is set up

	void resizeGL(int w, int h);// called every time the widget is resized

	void paintGL();// called every time the widget needs painting



	private:


	void Driver();

	// RUBIKS CUBE

	//FIRST 9 ( FRONT )
	void CubePiece1x1();
	void CubePiece2x1();
	void CubePiece3x1();
	void CubePiece4x1();
	void CubePiece5x1();
	void CubePiece6x1();
	void CubePiece7x1();
	void CubePiece8x1();
	void CubePiece9x1();

	//SECOND 9 ( MIDDLE )
	void CubePiece1x2();
	void CubePiece2x2();
	void CubePiece3x2();
	void CubePiece4x2();
	void CubePiece5x2();
	void CubePiece6x2();
	void CubePiece7x2();
	void CubePiece8x2();
	void CubePiece9x2();

	//THIRD 9 ( BACK )
	void CubePiece1x3();
	void CubePiece2x3();
	void CubePiece3x3();
	void CubePiece4x3();
	void CubePiece5x3();
	void CubePiece6x3();
	void CubePiece7x3();
	void CubePiece8x3();
	void CubePiece9x3();

	//	X Y Z	AXIS
	void CylinderX();
	void CylinderY();
	void CylinderZ();

	// TESTING CUBE
	void TEST();



// RUBIKS CUBE

//FIRST 9 ( FRONT )

GLUquadricObj* pCubePiece1;
GLUquadricObj* pCubePiece2;
GLUquadricObj* pCubePiece3;
GLUquadricObj* pCubePiece4;
GLUquadricObj* pCubePiece5;
GLUquadricObj* pCubePiece6;
GLUquadricObj* pCubePiece7;
GLUquadricObj* pCubePiece8;
GLUquadricObj* pCubePiece9;

//SECOND 9 ( MIDDLE )
GLUquadricObj* pCubePiece1x2;
GLUquadricObj* pCubePiece2x2;
GLUquadricObj* pCubePiece3x2;
GLUquadricObj* pCubePiece4x2;
GLUquadricObj* pCubePiece5x2;
GLUquadricObj* pCubePiece6x2;
GLUquadricObj* pCubePiece7x2;
GLUquadricObj* pCubePiece8x2;
GLUquadricObj* pCubePiece9x2;

//THIRD 9 ( BACK )
GLUquadricObj* pCubePiece1x3;
GLUquadricObj* pCubePiece2x3;
GLUquadricObj* pCubePiece3x3;
GLUquadricObj* pCubePiece4x3;
GLUquadricObj* pCubePiece5x3;
GLUquadricObj* pCubePiece6x3;
GLUquadricObj* pCubePiece7x3;
GLUquadricObj* pCubePiece8x3;
GLUquadricObj* pCubePiece9x3;
GLUquadricObj* pCylinderX;
GLUquadricObj* pCylinderY;
GLUquadricObj* pCylinderZ;
//AXIS




	int Move1; // parameter for rotations
	int Xrotation;   //slider rotation
	int Yrotation; //slider rotation


Image   _image;
Image2   _image2;
Image3   _image3;
Image4   _image4;
Image5   _image5;
Image6   _image6;
        QImage* p_qimage;

};

#endif
