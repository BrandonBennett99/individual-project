/****************************************************************************
** This file incluse coding for the features of the software explained below
** This code is part of Individual Project Software
** Antreas Josephides ID :201026404
*****************************************************************************/

#include "RubicCubeWindow.h"
#include <iostream>


RubicCubeWindow::RubicCubeWindow(QWidget *parent) // constructor / destructor
	: QWidget(parent)
{


	menuBar = new QMenuBar(this); 	// create menu bar

	fileMenu = menuBar->addMenu("&File");	// create file menu


	actionQuit = new QAction("&Quit", this); 	// create the action

	fileMenu->addAction(actionQuit); 	// add the item to the menu


	windowLayout = new QBoxLayout(QBoxLayout::TopToBottom, this); 	// create the window layout


	cubeWidget = new RubicWidget(this); 	// create main widget
	windowLayout->addWidget(cubeWidget);


	nSlider = new QSlider(Qt::Horizontal); 	// create slider
        connect(nSlider, SIGNAL(valueChanged(int)), cubeWidget, SLOT(updateYrotation(int)));
        windowLayout->addWidget(nSlider);

	nSlider2 = new QSlider(Qt::Horizontal); 	// create slider2
        connect(nSlider2, SIGNAL(valueChanged(int)), cubeWidget, SLOT(updateXrotation(int)));
        windowLayout->addWidget(nSlider2);


	ptimer = new QTimer(this); //create timer
        connect(ptimer, SIGNAL(timeout()),  cubeWidget, SLOT(updateMethod1Moves()));
        ptimer->start(0);

	ptimer2 = new QTimer(this); //create timer 2
        connect(ptimer, SIGNAL(timeout()),  cubeWidget, SLOT(updateMethod2Moves()));
        ptimer->start(0);
}

RubicCubeWindow::~RubicCubeWindow() // destructor
{
	delete ptimer;
	delete ptimer2;
	delete nSlider;
	delete nSlider2;
	delete cubeWidget;
	delete windowLayout;
	delete actionQuit;
	delete fileMenu;
	delete menuBar;
}


void RubicCubeWindow::ResetInterface() // resets all the interface elements
{
	nSlider->setMinimum(0);  			// Sets Minimum, Maximum and current value of Slider
	nSlider->setMaximum(360);
	nSlider->setValue(0);

	nSlider2->setMinimum(0);  			// Sets Minimum, Maximum and current value of Slider2
	nSlider2->setMaximum(360);
	nSlider2->setValue(0);


	cubeWidget->update(); 	// force refresh
	update();
}
